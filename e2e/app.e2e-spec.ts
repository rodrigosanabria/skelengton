import { SysadmPage } from './app.po';

describe('sysadm App', () => {
  let page: SysadmPage;

  beforeEach(() => {
    page = new SysadmPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
