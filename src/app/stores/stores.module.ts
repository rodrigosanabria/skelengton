
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { State, StoreModule } from '@ngrx/store';
import { ActionReducerMap } from '@ngrx/store';
import * as fromAuth from './auth/auth.index';
import { counterReducer } from './counter/counter.reducer';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../../environments/environment'; // Angular CLI environment

const reducers = {
 auth : fromAuth.auth.reducer,
 counter: counterReducer   
}

@NgModule({
    imports: [
        EffectsModule.forRoot(
            [fromAuth.auth.effects]
        ),
        StoreModule.forRoot(reducers),
         !environment.production ? StoreDevtoolsModule.instrument({ maxAge: 50 }) : []
    ],
    exports: [],
    declarations: [],
    providers: [],
})
export class StoresModule { }
