import { ActionReducer } from '@ngrx/store';
import { AuthState } from './auth.interfaces';
import { Action } from './Action';

const initalState : AuthState = {
    isLogged : false,
    user    : null
};

export function authReducer (state = initalState, action:Action): AuthState {

  switch(action.type) {
      
    case 'LOG_OUT': {
      return initalState
    }
    case 'LOG_IN_SUCCESS': {
      return { user: action.payload.user, isLogged: true }
    }
    case 'LOG_IN_FAILED': {
      return initalState;
    }
    case 'RESTORE_SESSION': {
      return {user: action.payload.user, isLogged: true };
    }
    default: {
            return state;
    }

  }
}