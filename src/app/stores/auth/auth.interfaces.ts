export interface User {
    email : string,
    password: string
}
export interface AuthState {
    isLogged : boolean;
    user: User | null 
}