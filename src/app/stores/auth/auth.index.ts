import { AuthEffects } from './auth.effects';
import { authReducer } from './auth.reducer';

export const auth =  {
 reducer : authReducer,
 effects : AuthEffects
}