import { User } from "./auth.interfaces";
import { Injectable } from "@angular/core";
import "rxjs/add/operator/map";
import { defer } from 'rxjs/observable/defer';
import { of } from 'rxjs/observable/of';
import { Action } from "./Action";
import { Observable } from "rxjs/Observable";
import { Effect, Actions } from "@ngrx/effects";

const users: Array<User> = [
  {
    email: "test@test.com",
    password: "test@"
  }
];

function findUser(email: string): User {
  return this.users.find((user: User) => {
    return user.email === email;
  });
}

@Injectable()
export class AuthEffects {

  @Effect()
  init$: Observable<Action> = defer(() => {
    return of({type: "default"});
  });

  @Effect()
  someEffect$: Observable<Action> = this.actions$
    .ofType("LOG_IN")
    .map((action: Action) => {
      let user = <User>action.payload;
      let userCompare = findUser(user.email);
      if (userCompare) {
        if (userCompare.password === user.password) {
          return { type: "LOG_IN_SUCCESS", payload: { user } };
        }
      }
      return { type: "LOG_IN_FAILED" };
    });

  constructor(private actions$: Actions) {}
}
