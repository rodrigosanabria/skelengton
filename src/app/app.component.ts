import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Rx';

import { Component } from '@angular/core';



interface AppState {
  counter : number
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  counter: Observable<number>;

	constructor(private store: Store<AppState>) {
		this.counter = store.select("counter");
	}

	increment(){
		this.store.dispatch({ type: 'INCREMENT' });
	}

	decrement(){
		this.store.dispatch({ type: 'DECREMENT' });
	}

	reset(){
		this.store.dispatch({ type: 'RESET' });
	}
}
